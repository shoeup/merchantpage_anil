import "./MerchantLandingPage.css";
import { useParams } from "react-router";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import image from "./logo.png";
import Footer from "../footer/Footer/Footer";

const MerchantLandingPage = () => {
  // let { mid } = useParams();
  // console.log(mid);
  let mid = 3;

  const navigate = useNavigate();
  const [name, setName] = useState("");
  const onNameChange = function (e) {
    const value = e.target.value;
    setName(value);
  };

  return (
    <div id="merchant-landing-page">
      <div id="merchant-page-header2">
        <span
          onClick={function () {
            navigate(`/all-products/${mid}`);
          }}
        >
          <img id="logo" src={image} alt="" srcset="" />
        </span>
        <span
          id="all-products"
          onClick={function () {
            navigate(`/all-products/${mid}`);
          }}
        >
          All Products
        </span>

        <span className="searchMerch">
          <input
            id="search-merchant"
            type="text"
            name="name"
            required
            value={name}
            onChange={onNameChange}
            placeholder="search by name.."
          />

          <span
            id="search-btn"
            onClick={function () {
              navigate(`/search-by-name/${mid}/${name}`);
            }}
          >
            Search
          </span>
        </span>

        <span
          id="add-product"
          onClick={function () {
            navigate(`/add-product/${mid}`);
          }}
        >
          Add Product
        </span>
        <a
          id="logout-from-merchant"
          style={{ cursor: "pointer" }}
          onClick={function () {
            localStorage.clear();
            navigate("/login");
          }}
        >
          {" "}
          Logout
        </a>
      </div>
      <br />
      <div id="merchant-page-header1">
        <span>Merchant Dashboard</span>
      </div>
    </div>
  );
};

export default MerchantLandingPage;
